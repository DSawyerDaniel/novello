
/*
 * Color swatch
 */
var swatch = {
	almond:"#efdecd",beige:"#f5f5dc",bone:"#e3dac9",champagne:"#f7e7ce",cream:"#fffdd0",eggshell:"#f0ead6",ivory:"#fffff0",linen:"#faf0e6",magnolia:"#f8f4ff",mintcream:"#f5fffa",ashgrey:"#b2beb5",charcoal:"#36454f",coolgrey:"#8c92ac",darkgrey:"#a9a9a9",grey:"#bebebe",lightgrey:"#d3d3d3",rosequartz:"#aa98a9",silver:"#c0c0c0",slate:"#708090",taupe:"#8b8589",azure:"#007fff",babyblue:"#89cff0",blueberry:"#4f86f7",cerulean:"#2a52be",cornflower:"#6495ed",denim:"#1560bd",indigo:"#091f92",lapislazuli:"#26619c",navy:"#000080",sky:"#87ceeb",aqua:"#00ffff",cyan:"#00b7eb",electric:"#7df9ff",mint:"#3eb489",pine:"#01796f",robinegg:"#00cccc",seafoam:"#2e8b57",teal:"#008080",tiffany:"#0abab5",turquoise:"#40e0d0",apple:"#8db600",asparagus:"#87a96b",chartreuse:"#dfff00",forest:"#014421",green:"#66b032",kelly:"#4cbb17",lime:"#bfff00",olive:"#808000",pistachio:"#93c572",shamrock:"#009e60",amber:"#ffbf00",banana:"#ffe135",citrine:"#e4d00a",daffodil:"#ffff31",gold:"#ffd700",lemon:"#fff700",mustard:"#ffdb58",saffron:"#f4c430",straw:"#e4d96f",yellow:"#fefe33",apricot:"#fbceb1",burnt:"#cc5500",carrot:"#ed9121",cinnamon:"#d2691e",flame:"#e25822",peach:"#ffcba4",pumpkin:"#ff7518",rust:"#b7410e",tangerine:"#f28500",tomato:"#ff6347",bronze:"#cd7f32",cafeaulait:"#a67b5b",camel:"#c19a6b",chocolate:"#7b3f00",coffee:"#6f4e37",copper:"#b87333"};

/*
 * Screen space objects
 */
var textObj = {
	//
	width: 500,
	height: 220,
	element: false,
	built: false,
	visible: false,
	create: function() {
		if (!this.built) {
			this.built = true;
			this.visible = true;
			var node = document.createElement("DIV");
			this.element = node;
			node.id = "text";
			node.style = `
				color:white;
				position:absolute;
				width:`+this.width+`px;
				height:`+this.height+`px;
				top:`+(scene.height - this.height - (20*2) - 20)+`px;
				left:`+((scene.width - this.width - (20*2))/2)+`px;
				margin:0px;
				padding:20px;
				background-color:rgba(20,20,20,0.7);`; // TODO
			novelloDiv.appendChild(node);
		}
	},
	setPos: function(x, y) {
		if (this.element) {
			this.element.style.left = x;
			this.element.style.top = y;
		}
	},
	hide: function() {
		if (this.element) {
			if (this.visible) {
				this.element.style.display = "hidden";
				this.visible = false;
				console.log("textObj.hide()");
			} else {
				console.warn("textObj.hide() already hidden", "(story.case:"+scene.pos[1]+")")
			}
		}
	},
	show: function() {
		if (this.element) {
			this.element.style.display = "block";
			this.visible = true;
			console.log("textObj.show()");
		}
	},
};
var choiceObj = {
	//
	x: 0,
	y: 0,
};

/*
 * Scene object
 */
var scene = {
	width: 1280,
	height: 720,
	element: false,
	built: false,
	visible: false,
	pos: [0,0,1],
	atLoc: {name:"worldspace",url:"images/world.png"},
	locList: [],
	chars: [],
	create: function() {
		if (!this.built) {
			this.built = true;
			this.visible = true;
			var node = document.createElement("DIV");
			this.element = node;
			node.id = "scene";
			node.style = `
				position:absolute;
				width:`+this.width+`px;
				height:`+this.height+`px;
				top:0px;
				left:0px;
				margin:0px;
				padding:0px;
				background:`+swatch.beige+`;`; // TODO
			novelloDiv.appendChild(node);
		}
	},
	hide: function() {
		if (this.element) {
			if (this.visible) {
				this.element.style.display = "hidden";
				this.visible = false;
				console.log("textObj.hide()");
			} else {
				console.warn("textObj.hide() already hidden", "(story.case:"+scene.pos[1]+")")
			}
		}
	},
	show: function() {
		if (this.element) {
			this.element.style.display = "block";
			this.visible = true;
			console.log("textObj.show()");
		}
	},
	updateScenePos: function() {
		this.pos[2] = this.pos[1] + 1;
		if ((this.pos[0] = this.pos[1] - 1) < 0) {
			this.pos[0] = 0;
		}
		story(this.pos[1]);
	},
	next: function() {
		this.pos[1] = this.pos[2];
		this.updateScenePos();
	},
	prev: function() {
		this.pos[1] = this.pos[0];
		this.updateScenePos();
	},
	hasLoc: function(l) { //TODO
		for (var i=0;i<this.locList.length;i++) {
			if (this.locList[i] == l) {
				return true;
			}
		}
		return false;
	},
	remLoc: function(l) { //TODO
		//
	},
	addLoc: function(l) {
		inList = this.hasLoc(l);
		if (!inList) {
			console.log("addLoc() added %c%s", "color:green;", l.name, "to locations");
			this.locList.push(l);
		} else {
			console.warn("addLoc() %c%s", "color:green;", l.name, "already exists",
				"(story.case:"+this.pos[1]+")");
		}
	},
	changeLoc: function(l) { //TODO
		if (this.atLoc == l) {
			console.warn("changeLoc() already at location %c%s", "color:green;", l.name,
				"(story.case:"+this.pos[1]+")");
		} else {
			inList = this.hasLoc(l);
			if (inList) {
				console.log("changeLoc() changed location to %c%s", "color:green;", l.name);
				this.atLoc = l;
			} else {
				this.addLoc(l);
				this.changeLoc(l);
			}
		}
	},
	update: function() {
		// TODO:
		// how characters images are aligned
		var charNum = this.chars.length;
		for (var i=0;i<charNum;i++){
			var c = this.chars[i];
			var node = c.element;
			if (!node) {
				node = document.createElement("DIV");
				node.style = `
					margin: 0px;
					padding: 0px;
					position: absolute;
				`;
				c.element = node;
				scene.element.appendChild(node);
				node.innerHTML = "new";
				console.log("update() creating node for", c.name);
			}
			node.innerHTML = "<img height='"+scene.height+"px' src='novello/"+c.mood.url+"'>";
			switch(i) {
				case 0: node.style.left="50px";node.style.transform="scaleX(1)";break;
				case 1: node.style.left="880px";node.style.transform="scaleX(-1)";break;
				case 2: node.style.left="200px";node.style.transform="scaleX(1)";break;
				case 3: node.style.left="650px";node.style.transform="scaleX(-1)";break;
			}
		}
		if (charNum < 1) {
			if (this.element) {
				this.element.innerHTML = "";
			}
			console.log("update() scene is empty");
		}
	},
	hasChar: function(c) {
		if (c.element) {
			return true;
		}
		return false;
	},
	addChar: function(c) {
		if (c.element) {
			console.log("addChar() %c%s", "color:"+c.color+";", c.name, "is already in the scene");
		} else {
			console.log("addChar() added %c%s", "color:"+c.color+";", c.name, "to scene");
			this.chars.push(c);
		}
		this.update();
	},
	remChar: function(c) {
		if (c.element) {
			console.log("remChar() removed %c%s", "color:"+c.color+";", c.name, "from the scene");
			var charNum = this.chars.length;
			for (var i=0;i<charNum;i++) {
				if (c == this.chars[i]) {
					scene.element.removeChild(c.element);
					c.element = false;
					this.chars.splice(i,1);
				}
			}
		} else {
			console.warn("remChar() %c%s", "color:"+c.color+";", c.name, "is not in the scene",
				"(story.case:"+this.pos[1]+")");
		}
		this.update();
	},
	vacate: function() {
		var charNum = this.chars.length;
		for (var i=0;i<charNum;i++) {
			var c = this.chars[i]
			c.element = false;
			console.log("vacate() removed %c%s", "color:"+c.color+";", c.name, "from the scene");
		}
		this.chars.length = 0;
		this.update();
	},
}

/*
 * Character object creator
 */
function character (name, color) {
	this.element = false;
	this.name = name;
	this.color = color;
	this.mood = {name:"shadow",url:"image/shadow.png"};
	this.moodList = [];
	this.setName = function(n) {
		this.name = n;
	};
	this.setColor = function(c) {
		this.color = c;
	};
	this.hasMood = function(m) {
		for (var i=0;i<this.moodList.length;i++){
			if (this.moodList[i].name == m) {
				return true;
			}
		}
		return false;
	};
	this.addMood = function(m, u) {
		this.moodList.push({name:m,url:u});
		if (this.moodList.length == 1) {
			this.mood = this.moodList[0];
		}
	};
	this.setMood = function(m) {
		for (var i=0;i<this.moodList.length;i++){
			if (this.moodList[i].name == m) {
				this.mood = this.moodList[i];
				console.log("setMood() %c%s %cis now %c%s",
				"color:"+this.color+";",
				this.name,
				"color: black",
				"color: blue",
				m);
			}
		}
	};
	this.speak = function(str) { //TODO: handle adding msg to text box
		if (!this.element) {
			scene.addChar(this);
		}
		textObj.element.innerHTML = "<p>"+str+"</p>";
		console.log("speak() %c%s%c ("+this.mood.name+") said %c%s", "color:"+this.color+";",
		 this.name, "color: black", "color: blue", str);
	};
	this.showMood = function(m) { //TODO: handle showing image on scene
		this.setMood(m);
		if (!this.element) {
			scene.addChar(this);
			scene.update();
		}
	};
}

/*
 * Location object creator
 */
function place (name, url) {
	this.name = name;
	this.url = url;
	this.changeName = function(n) {
		this.name = n;
	};
	this.changeURL = function(u) {
		this.url = u;
	};
}

/*
 * Starts Novello
 */
var novelloDiv;
function startNovello() {
	console.log("%cNovello started...", "color: green");

	novelloDiv = document.getElementById("novello");

	scene.create();
	textObj.create();

	//TESTING
	var max = 10;
	for (var i=0; i<(max+1); i++) {
		scene.next();
	}
}