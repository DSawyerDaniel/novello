// You can define your own characters
//
//		var d = new character("Dev", "#000000");

var testa = new character("Testo", "#00b7eb");
var testb = new character("Tosto", "#ac3000");

// Define images associated with your character
//
//		d.addMood("name", "url");

testa.addMood("neutral", "images/neutral.png");
testa.addMood("happy", "images/happy.png");
testa.addMood("angry", "images/angry.png");

// Then call those images
//
//		d.showMood("neutral");



//
function story(i) {
	switch(i) {

		/*
		 * Your story goes here
		 */
		case 0: // number determines order
		testa.speak("Hey there!");
		break; // always include

		case 1:
		testa.speak("How's it goin'?");
		break;

		case 2:
		scene.remChar(testb);
		scene.remChar(testa);
		break;

		case 3:
		break;

		case 4:
		break;

		case 5:
		break;

		case 6:
		break;

		case 7:
		break;

		case 8:
		break;

		case 9:
		break;

		case 10:
		break;

		/*
		 * End of your story
		 */

		default:
			console.log("%cNo story found.", "color: green");
			break;
	}
}